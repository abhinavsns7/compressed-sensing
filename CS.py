import numpy as np
import scipy as sp
from skimage import io as skio
from skimage import transform as sktf
import matplotlib.pyplot as plt


def main():

    im = skio.imread('nmr.png', as_gray=True)

    im = sktf.rescale(im, 2, anti_aliasing=False)

    plt.figure()
    plt.imshow(im, cmap='gray')
    plt.title('original')

    ft = np.fft.fft2(im)

    mag = np.log(1 + np.abs(np.fft.fftshift(ft)))

    plt.figure()
    plt.subplot(1, 2, 1)
    plt.imshow(mag, cmap='gray')
    plt.title('magnitude spectrum')

    phase = np.angle(np.fft.fftshift(ft))

    plt.subplot(1,2,2)
    plt.imshow(phase, cmap='gray')
    plt.title('phase spectrum')
    plt.show()



if __name__ == '__main__':
    main()
